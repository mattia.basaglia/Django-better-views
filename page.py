"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

import re

from django.shortcuts import render
from django.conf.urls import url
from django.urls import reverse_lazy
from django.utils.html import format_html
from django.template.loader import render_to_string
from django.contrib.staticfiles.storage import staticfiles_storage


def _camel_sub(camel):
    for regex in regexes:
        camel = re.sub(regex, r'\1_\2', camel)
    return camel
regexes = ['([A-Z]+)([A-Z][a-z])', '([a-z0-9])([A-Z])', "(\S)\s+(\S)"]


def snake_case(camel):
    """
    Converts a CamelCase string into snake_case
    """
    return _camel_sub(camel).lower()


def humanize(slug):
    """
    Converts a CamelOr_snake string into a Human Readable Title
    """
    return " ".join(
        x.capitalize() if not x.isupper() else x
        for x in re.split("[\s_]+", _camel_sub(slug))
    )


class Link(object):
    """
    Class representing an item hyperlink
    """
    def __init__(self, url, text, condition=None):
        self.url = url
        self.text = text
        if type(condition) is str or type(condition) is unicode:
            self.condition = lambda request: hasattr(request, "user") and \
                                             request.user.has_perm(condition)
        else:
            self.condition = condition

    def visible(self, request):
        """
        Whether the link should be displayed
        """
        return not self.condition or self.condition(request)


class LinkGroup(object):
    """
    A group of links (eg: menus, footers etc
    """
    def __init__(self, title="", links=None):
        """
        Available "overloads":
            title (str), links (list of Link)
            links (list of Link)
        """
        if type(title) is list and not links:
            self.links = title
            self.title = ""
        else:
            self.title = title
            self.links = links if links is not None else []

    def add_link(self, *links):
        """
        Appends each argument (which shall be Link object) to the links
        """
        for link in links:
            self.links.append(link)

    def __iter__(self):
        return iter(self.links)

    def __contains__(self, url):
        if isinstance(url, Link):
            url = url.url
        return any(link.url == url for link in self.links)

    def __nonzero__(self):
        return len(self.links)


class Resource(object):
    """
    An html resource for a page
    """

    # Load flags:
    Url      = 0x01 # The resource links to this url when rendered
    Template = 0x02 # The resource includes the remplate when rendered
    Text     = 0x04 # The resource includes this hardcoded value when rendered
    Static   = 0x08 # The resource links to the given static url
    # Type flags
    Script   = 0x10 # The resource is a script
    Style    = 0x20 # The resource is stylesheet
    Custom   = 0x40 # The resource is some custom HTML string
    Icon     = 0x80 # Favicon

    def __init__(self, flags, target):
        self.flags = flags
        self.target = target

    def render(self, context):
        """
        Rerturns the html corresponding to this resource
        """
        return format_html(self._template(), self._contents(context))

    def _contents(self, context):
        if self.flags & (Resource.Url | Resource.Text):
            return self.target
        elif self.flags & Resource.Template:
            return render_to_string(self.target, context)
        elif self.flags & Resource.Static:
            return staticfiles_storage.url(self.target)
        raise ValueError("Unknown flags")

    def _template(self):
        if self.flags & Resource.Script:
            if self._is_embedded():
                return "<script>{}</script>"
            else:
                return "<script src='{}'></script>"
        elif self.flags & Resource.Style:
            if self._is_embedded():
                return "<style>{}</style>"
            else:
                return "<link rel='stylesheet' href='{}'/>"
        elif self.flags & Resource.Icon:
            return "<link rel='icon' href='{}'/>"
        elif self.flags & Resource.Custom:
            return "{}"
        raise ValueError("Unknown flags")

    def _is_embedded(self):
        return self.flags & (Resource.Text | Resource.Template)


class Page(object):
    """
    Class used to represent pages, can also be used as class for an alternative
    to Django class-based views.
    """
    site_name = ""
    template_root = "simple_page/"
    base_template = template_root + "base.html"
    footer = [] # List of link groups
    menu = LinkGroup()
    breadcrumbs = LinkGroup()
    title = None
    block_contents = ""
    resources = [Resource(
        Resource.Style|Resource.Static,
        template_root + "style.css"
    )]
    title_template = "{title}"
    decorators = []

    def __init__(self):
        if self.title is None:
            self.title = humanize(self.__class__.__name__)
        self.resources = list(self.resources)

    def context(self, request):
        """
        Returns the context dict to be used for rendering
        """
        return {}

    def render(self, request, extra_context={}, status_code=None):
        """
        Returns the HTML string corresponding to this page
        """
        context = {
            "page": self
        }
        context["context"] = context
        context.update(self.context(request))
        context.update(extra_context)

        return render(request, self.base_template, context, status_code)

    def reply(self, request):
        """
        Sends a response based on the request
        """
        return self.render(request)

    @classmethod
    def url(cls, *args, **kwargs):
        """
        Returns a (lazy) url for this class
        """
        return reverse_lazy(cls.slug(), args=args, kwargs=kwargs)

    @classmethod
    def slug(cls):
        """
        Returns a slug for this page (based on the class name)
        """
        return snake_case(cls.__name__)

    @classmethod
    def link(cls, title=None, condition=None):
        """
        Returns a link for this page.

        It assumes it has been registered using url_pattern()
        """
        return Link(
            cls.url(),
            title or cls.title or humanize(cls.__name__),
            condition
        )

    @classmethod
    def url_pattern(cls, pattern, *args, **kwargs):
        """
        Returns an url pattern for this page
        """
        view = cls.view
        for decorator in cls.decorators + kwargs.pop("decorators", []):
            view = decorator(view)
        return url(pattern, view, name=cls.slug(), *args, **kwargs)

    @classmethod
    def view(cls, request, *args, **kwargs):
        """
        This can be used as a view function
        """
        return cls(*args, **kwargs).reply(request)

    def html_title(self):
        """
        Uses title_template to render the HTML title
        """
        context = {}
        context.update(vars(self.__class__))
        context.update(vars(self))
        return format_html(self.title_template, **context)


class ViewRegistry(object):
    """
    Object to register url patterns to expose
    """
    def __init__(self):
        self._items = []

    def register(self, *args, **kwargs):
        """
        Decorator for Page subclasses, that registers their url_pattern().

        Arguments are forwarded to the decorated class.url_pattern.
        This uses the default weight of 0.
        """
        return self.weighted_register(0.0, *args, **kwargs)

    def weighted_register(self, weight, *args, **kwargs):
        """
        Decorator for Page subclasses, that registers their url_pattern().

        \param weight Order weight, higher weight urls are registered later.

        Other arguments are forwarded to the decorated class.url_pattern.
        """
        def register(cls):
            self.add_pattern(cls.url_pattern(*args, **kwargs), weight)
            return cls
        return register

    def add_pattern(self, url_pattern, weight=0.0):
        """
        Registers a url pattern.
        \param url_pattern  Url pattern to register. It can be anything you'd
                            use in urlpatterns or a ViewRegistry instance.
        \param weigh        Order weight, higher weight urls are registered later.
        """
        self._items.append((weight, len(self._items), url_pattern))

    def add_url(self, *args, **kwargs):
        """
        Forwards arguments to django.conf.urls.url and registers it
        with the default weight of 0.
        """
        self.weighted_add_url(0.0, *args, **kwargs)

    def weighted_add_url(self, weight, *args, **kwargs):
        """
        Forwards arguments to django.conf.urls.url and registers it.

        \param weight Order weight, higher weight urls are registered later.

        Other arguments are forwarded to the decorated django.conf.urls.url.
        """
        self.add_pattern(url(*args, **kwargs), weight)

    @property
    def url_patterns(self):
        """
        List of registered url patterns
        """
        return sum(
            (self._url_patterns(item) for item in sorted(self._items)),
            []
        )

    def _url_patterns(self, item):
        """
        Extract a list of url patterns from an element of self._items
        """
        if isinstance(item[2], ViewRegistry):
            return item[2].url_patterns
        return [item[2]]
