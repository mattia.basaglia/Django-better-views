Better Django Views
===================

Provides a better and simpler to use framework for class-based views.


License
-------

AGPLv3+, see COPYING


Installation
------------

Add the repo as an app to your Django project, then inherit the appropriate
base classes.


Sources
-------

See https://gitlab.com/mattia.basaglia/Django-better-views


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>
