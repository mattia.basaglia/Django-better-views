"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import re

from mock import patch

from django.test import SimpleTestCase
from django.test.client import RequestFactory

from .. import api_base as api


class TestJsonResponse(SimpleTestCase):
    def test_to_string(self):
        self.assertEqual(
            api.JsonResponse.to_string({"foo": 123, "bar": [1, 2, 3]}, False),
            '{"foo": 123, "bar": [1, 2, 3]}'
        )
        pretty = api.JsonResponse.to_string({"foo": 123, "bar": [1, 2, 3]}, True)
        self.assertEqual(
            re.sub(" $", "", pretty, flags=re.MULTILINE),
            '''{
    "foo": 123,
    "bar": [
        1,
        2,
        3
    ]
}'''
        )

    def test_attributes(self):
        self.assertEqual(api.JsonResponse.mime_type, "application/json")
        self.assertEqual(api.JsonResponse.human_name, "JSON")

    def test_error(self):
        self.assertEqual(
            api.JsonResponse.error(404).content,
            '{"error": "Not Found"}'
        )
        self.assertEqual(
            api.JsonResponse.error(404, "Newt Fond").content,
            '{"error": "Newt Fond"}'
        )


class TestPonResponse(SimpleTestCase):
    def test_to_string(self):
        self.assertEqual(
            api.PonResponse.to_string({"foo": 123, "bar": [1, 2, 3]}, False),
            "{'foo': 123, 'bar': [1, 2, 3]}"
        )

    def test_attributes(self):
        self.assertEqual(api.PonResponse.mime_type, "text/plain")
        self.assertEqual(api.PonResponse.human_name, "Python Object Notation")


class TestXmlResponse(SimpleTestCase):
    def test_to_string_scalars(self):
        self.assertEqual(
            api.XmlResponse.to_string(123, False),
            '<?xml version="1.0" ?><results>123</results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string("hello", False),
            '<?xml version="1.0" ?><results>hello</results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string("<hello>", False),
            '<?xml version="1.0" ?><results>&lt;hello&gt;</results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string(12.3, False),
            '<?xml version="1.0" ?><results>12.3</results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string(True, False),
            '<?xml version="1.0" ?><results>True</results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string(None, False),
            '<?xml version="1.0" ?><results>None</results>'
        )

    def test_to_string_dict(self):
        self.assertEqual(
            api.XmlResponse.to_string({"foo": 123, "bar": "hello"}, False),
            '<?xml version="1.0" ?><results><foo>123</foo><bar>hello</bar></results>'
        )

    def test_to_string_list(self):
        self.assertEqual(
            api.XmlResponse.to_string(["foo", "bar"], False),
            '<?xml version="1.0" ?><results><result>foo</result><result>bar</result></results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string(("foo", "bar"), False),
            '<?xml version="1.0" ?><results><result>foo</result><result>bar</result></results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string({"foo", "bar"}, False),
            '<?xml version="1.0" ?><results><result>foo</result><result>bar</result></results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string({"foo": ["foo", "bar"]}, False),
            '<?xml version="1.0" ?><results><foo><foo>foo</foo><foo>bar</foo></foo></results>'
        )
        self.assertEqual(
            api.XmlResponse.to_string({"s": ["foo", "bar"]}, False),
            '<?xml version="1.0" ?><results><s><s>foo</s><s>bar</s></s></results>'
        )

    def test_attributes(self):
        self.assertEqual(api.XmlResponse.mime_type, "text/xml")
        self.assertEqual(api.XmlResponse.human_name, "XML")

    def test_to_string_pretty(self):
        self.assertEqual(
            api.XmlResponse.to_string({"foo": "bar"}, True),
           '<?xml version="1.0" ?>\n<results>\n\t<foo>bar</foo>\n</results>\n'
        )


class TestViewWrapper(SimpleTestCase):
    class Class:
        response_types = {
            "json": api.JsonResponse,
            "xml": api.XmlResponse
        }
        default_response = api.JsonResponse

        def method(self, *args, **kwargs):
            return "foo" + str(args) + str(kwargs)

    def test_call(self):
        wrapper = api.BoundViewWrapper(self.Class(), self.Class.method)
        self.assertEqual(wrapper(), "foo(){}")
        self.assertEqual(wrapper("bar"), "foo('bar',){}")

    def test_url_pattern(self):
        self.assertEqual(
            api.BoundViewWrapper(self.Class(), self.Class.method)
            .url_pattern("foo"),
            "^foo\.(?P<type>xml|json)$"
        )

    def test_url_pattern_name(self):
        self.assertEqual(
            api.BoundViewWrapper(self.Class(), self.Class.method, "bar")
            .url_pattern("foo"),
            "^bar\.(?P<type>xml|json)$"
        )

    def test_view_unknown_type(self):
        wrap = api.BoundViewWrapper(self.Class(), self.Class.method)
        req = RequestFactory().get("")
        resp = wrap.view(req, "foo type")
        self.assertEqual(resp.status_code, 404)

    def test_view_wrong_method(self):
        wrap = api.BoundViewWrapper(self.Class(), self.Class.method)
        wrap.methods = ["POST"]
        req = RequestFactory().get("")
        resp = wrap.view(req, "json")
        self.assertEqual(resp.status_code, 405)

    def test_view_success(self):
        wrap = api.BoundViewWrapper(self.Class(), self.Class.method)
        req = RequestFactory().get("")
        resp = wrap.view(req, "json")
        self.assertEqual(resp.status_code, 200)

    def test_view_404_exception(self):
        def raise_exc(self):
            raise api.http.Http404()
        wrap = api.BoundViewWrapper(self.Class(), raise_exc)
        req = RequestFactory().get("")
        resp = wrap.view(req, "json")
        self.assertEqual(resp.status_code, 404)

    def test_view_500_exception(self):
        def raise_exc(self):
            raise Exception()
        wrap = api.BoundViewWrapper(self.Class(), raise_exc)
        req = RequestFactory().get("")
        with self.settings(DEBUG=False):
            resp = wrap.view(req, "json")
            self.assertEqual(resp.status_code, 500)

    def test_view_500_exception_debug(self):
        def raise_exc(self):
            raise Exception("foo")
        wrap = api.BoundViewWrapper(self.Class(), raise_exc)
        req = RequestFactory().get("")
        with self.settings(DEBUG=True):
            self.assertRaisesMessage(Exception, "foo", lambda: wrap.view(req, "json"))

    def test_unbound(self):
        unbound = api.ViewWrapper(self.Class.method)
        obj = self.Class()
        bound = unbound.bound(obj)
        self.assertIsInstance(bound, api.BoundViewWrapper)
        self.assertIs(bound.this, obj)

    def test_url(self):
        wrap = api.BoundViewWrapper(self.Class(), self.Class.method)
        with patch.object(api, "url") as url:
            wrap.url("foo")
            url.assert_called_with(
                wrap.url_pattern("foo"),
                wrap.view,
                name="foo"
            )

    def test_decorator(self):
        class OtherClass:
            @api.view
            def foo(self):
                return "foo"

            @api.view("bar123")
            def bar(self):
                return "bar"

        obj = OtherClass()

        bound_foo = OtherClass.foo.bound(obj)
        self.assertIsInstance(bound_foo, api.BoundViewWrapper)
        self.assertIs(bound_foo.this, obj)
        self.assertEqual(bound_foo(), "foo")

        bound_bar = OtherClass.bar.bound(obj)
        self.assertIsInstance(bound_bar, api.BoundViewWrapper)
        self.assertIs(bound_bar.this, obj)
        self.assertEqual(bound_bar(), "bar")
        self.assertEqual(bound_bar.pattern, "bar123")


class TestApiBase(SimpleTestCase):
    maxDiff = None

    class Derived(api.ApiBase):
        @api.view
        def foo(self):
            return "foo"

        @api.view("bar123")
        def bar(self):
            return "bar"

    def test_unbound(self):
        self.assertIsInstance(TestApiBase.Derived.foo, api.ViewWrapper)
        self.assertIsInstance(TestApiBase.Derived.bar, api.ViewWrapper)

    def test_bound(self):
        instance = TestApiBase.Derived(1)
        self.assertIsInstance(instance.foo, api.BoundViewWrapper)
        self.assertIsInstance(instance.bar, api.BoundViewWrapper)
        self.assertEqual(instance.foo(), "foo")
        self.assertEqual(instance.bar(), "bar")

    def test_url_patterns(self):
        instance = TestApiBase.Derived(1)
        with patch.object(api, "url", side_effect=lambda *a, **k: (a, k)):
            patterns, ns = instance.url_patterns()
        self.assertEqual(ns, "api")
        fallback_pattern = "(?P<name>.*?)(?:\.(?P<type>%s))?$" % \
            "|".join(instance.response_types)
        self.assertListEqual(
            patterns,
            [
                (
                    (instance.foo.url_pattern("foo"), instance.foo.view),
                    {"name": "foo"}
                ),
                (
                    (instance.bar.url_pattern("bar123"), instance.bar.view),
                    {"name": "bar"}
                ),
                (
                    (fallback_pattern, instance.not_found), {}
                ),
            ]
        )

    def test_not_found(self):
        instance = TestApiBase.Derived(1)
        resp = instance.not_found(RequestFactory().get(""), "foo", "json")
        self.assertEqual(resp.status_code, 404)
