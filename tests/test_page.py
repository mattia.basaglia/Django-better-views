"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

from mock import MagicMock, patch

from django.test import SimpleTestCase
from django.test.client import RequestFactory

from .. import page


def _mocked_reverse(name):
    return "http://" + name


class TestStingFuncs(SimpleTestCase):
    def test_snake_case(self):
        self.assertEqual(page.snake_case("foo"), "foo")
        self.assertEqual(page.snake_case("foo_bar"), "foo_bar")
        self.assertEqual(page.snake_case("FooBar"), "foo_bar")
        self.assertEqual(page.snake_case("FooBAR"), "foo_bar")
        self.assertEqual(page.snake_case("FOOBar"), "foo_bar")
        self.assertEqual(page.snake_case("Foo Bar"), "foo_bar")

    def test_humanize(self):
        self.assertEqual(page.humanize("foo_bar"), "Foo Bar")
        self.assertEqual(page.humanize("FooBar"), "Foo Bar")
        self.assertEqual(page.humanize("FooBAR"), "Foo BAR")
        self.assertEqual(page.humanize("FOOBar"), "FOO Bar")
        self.assertEqual(page.humanize("Foo Bar"), "Foo Bar")


class TestLink(SimpleTestCase):
    def test_visible(self):
        factory = RequestFactory()
        simple_get = factory.get('')

        self.assertTrue(page.Link("", "").visible(simple_get))

        link_lambda = page.Link("", "", lambda x: x is simple_get)
        self.assertTrue(link_lambda.visible(simple_get))
        self.assertFalse(link_lambda.visible(factory.get('')))

    def test_visible_perms(self):
        factory = RequestFactory()

        link_perm = page.Link("", "", "foo")
        self.assertFalse(link_perm.visible(factory.get('')))

        user = MagicMock()
        user.has_perm = lambda x: x == "foo"
        request_user = factory.get('')
        request_user.user = user

        self.assertTrue(link_perm.visible(request_user))

        user.has_perm = lambda x: x == "bar"
        self.assertFalse(link_perm.visible(request_user))


class TestLinkGroup(SimpleTestCase):
    def test_ctor(self):
        self.assertEqual(page.LinkGroup().title, "")
        self.assertListEqual(page.LinkGroup().links, [])

        self.assertEqual(page.LinkGroup("foo").title, "foo")
        self.assertListEqual(page.LinkGroup("foo").links, [])

        self.assertEqual(page.LinkGroup([1]).title, "")
        self.assertListEqual(page.LinkGroup([1]).links, [1])

        self.assertEqual(page.LinkGroup("foo", [1]).title, "foo")
        self.assertListEqual(page.LinkGroup("foo", [1]).links, [1])

    def test_contains(self):
        group = page.LinkGroup([page.Link("foo", "bar"), page.Link("hello", "world")])
        self.assertIn("foo", group)
        self.assertIn(page.Link("foo", "bar"), group)
        self.assertIn(page.Link("foo", "foo"), group)

        self.assertNotIn("bar", group)
        self.assertNotIn(page.Link("bar", "bar"), group)

    def test_add_link(self):
        group = page.LinkGroup()

        group.add_link(page.Link("foo", "bar"))
        self.assertIn("foo", group)

        group.add_link(page.Link("hello", "hello"), page.Link("world", "world"))
        self.assertIn("hello", group)
        self.assertIn("world", group)

        self.assertListEqual(page.LinkGroup().links, [])

    def test_iter(self):
        self.assertListEqual(list(page.LinkGroup([1, 2, 3])), [1, 2, 3])

    def test_bool(self):
        self.assertFalse(page.LinkGroup())
        self.assertFalse(page.LinkGroup("foo"))
        self.assertTrue(page.LinkGroup(["foo"]))


class TestResource(SimpleTestCase):
    def test_script(self):
        res = page.Resource(page.Resource.Script|page.Resource.Url, "foo")
        self.assertEqual(res.render({}), "<script src='foo'></script>")

        res = page.Resource(page.Resource.Script|page.Resource.Template, "foo")
        with patch.object(page, "render_to_string") as render_to_string:
            render_to_string.return_value = "bar"
            self.assertEqual(res.render({"foo": "bar"}), "<script>bar</script>")
            render_to_string.assert_called_with("foo", {"foo": "bar"})

        res = page.Resource(page.Resource.Script|page.Resource.Text, "foo")
        self.assertEqual(res.render({}), "<script>foo</script>")

        res = page.Resource(page.Resource.Script|page.Resource.Static, "foo")
        with patch.object(page, "staticfiles_storage") as staticfiles_storage:
            staticfiles_storage.url.return_value = "bar"
            self.assertEqual(res.render({}), "<script src='bar'></script>")
            staticfiles_storage.url.assert_called_with("foo")

    def test_style(self):
        res = page.Resource(page.Resource.Style|page.Resource.Url, "foo")
        self.assertEqual(res.render({}), "<link rel='stylesheet' href='foo'/>")

        res = page.Resource(page.Resource.Style|page.Resource.Template, "foo")
        with patch.object(page, "render_to_string") as render_to_string:
            render_to_string.return_value = "bar"
            self.assertEqual(res.render({"foo": "bar"}), "<style>bar</style>")
            render_to_string.assert_called_with("foo", {"foo": "bar"})

        res = page.Resource(page.Resource.Style|page.Resource.Text, "foo")
        self.assertEqual(res.render({}), "<style>foo</style>")

        res = page.Resource(page.Resource.Style|page.Resource.Static, "foo")
        with patch.object(page, "staticfiles_storage") as staticfiles_storage:
            staticfiles_storage.url.return_value = "bar"
            self.assertEqual(res.render({}), "<link rel='stylesheet' href='bar'/>")
            staticfiles_storage.url.assert_called_with("foo")

    def test_custom(self):
        res = page.Resource(page.Resource.Custom|page.Resource.Url, "foo")
        self.assertEqual(res.render({}), "foo")

        res = page.Resource(page.Resource.Custom|page.Resource.Template, "foo")
        with patch.object(page, "render_to_string") as render_to_string:
            render_to_string.return_value = "bar"
            self.assertEqual(res.render({"foo": "bar"}), "bar")
            render_to_string.assert_called_with("foo", {"foo": "bar"})

        res = page.Resource(page.Resource.Custom|page.Resource.Text, "foo")
        self.assertEqual(res.render({}), "foo")

        res = page.Resource(page.Resource.Custom|page.Resource.Static, "foo")
        with patch.object(page, "staticfiles_storage") as staticfiles_storage:
            staticfiles_storage.url.return_value = "bar"
            self.assertEqual(res.render({}), "bar")
            staticfiles_storage.url.assert_called_with("foo")

    def test_icon(self):
        res = page.Resource(page.Resource.Icon|page.Resource.Url, "foo")
        self.assertEqual(res.render({}), "<link rel='icon' href='foo'/>")

    def test_error(self):
        res = page.Resource(page.Resource.Custom, "foo")
        self.assertRaisesMessage(ValueError, "Unknown flags", lambda: res.render({}))

        res = page.Resource(page.Resource.Url, "foo")
        self.assertRaisesMessage(ValueError, "Unknown flags", lambda: res.render({}))


class TestPage(SimpleTestCase):
    def test_ctor(self):
        pageobj = page.Page()
        self.assertEqual(pageobj.title, "Page")
        self.assertIsNot(pageobj.resources, page.Page.resources)
        self.assertListEqual(pageobj.resources, page.Page.resources)

    def test_render(self):
        pageobj = page.Page()
        request = RequestFactory().get('')
        pageobj.rendered = 0

        def on_check(req, template, context, status_code):
            self.assertIs(req, request)
            self.assertEqual(template, pageobj.base_template)
            self.assertIs(context["page"], pageobj)
            self.assertIs(context["context"], context)
            pageobj.rendered += 1

        with patch.object(page, "render", side_effect=on_check):
            pageobj.render(request)
            self.assertEqual(pageobj.rendered, 1)
            pageobj.reply(request)
            self.assertEqual(pageobj.rendered, 2)
            with patch.object(page.Page, "__new__", return_value=pageobj):
                page.Page.view(request)
                self.assertEqual(pageobj.rendered, 3)

    def test_slug(self):
        self.assertEqual(page.Page.slug(), "page")

    def test_link(self):
        with patch.object(page, "reverse_lazy", side_effect=_mocked_reverse):
            pageobj = page.Page()
            link = pageobj.link()
            self.assertEqual(link.url, "http://page")
            self.assertEqual(link.text, pageobj.title)

            link = pageobj.link("Foo")
            self.assertEqual(link.url, "http://page")
            self.assertEqual(link.text, "Foo")

            pageobj.title = None
            link = pageobj.link()
            self.assertEqual(link.url, "http://page")
            self.assertEqual(link.text, "Page")

    def test_url_pattern(self):
        with patch.object(page, "url") as url_func:
            pageobj = page.Page()
            pageobj.url_pattern("foo")
            url_func.assert_called_with("foo", pageobj.view, name=pageobj.slug())

    def test_html_title(self):
        pageobj = page.Page()
        self.assertEqual(pageobj.html_title(), pageobj.title)
        pageobj.title_template = "<b>{title}</b>"
        self.assertEqual(pageobj.html_title(), "<b>Page</b>")


class TestViewRegistry(SimpleTestCase):
    def test_add_pattern_unweighted(self):
        registry = page.ViewRegistry()
        registry.add_pattern("foo")
        registry.add_pattern("bar")
        self.assertListEqual(registry.url_patterns, ["foo", "bar"])

    def test_add_pattern_weighted(self):
        registry = page.ViewRegistry()
        registry.add_pattern("foo", 1)
        registry.add_pattern("bar")
        registry.add_pattern("baz", -1)
        registry.add_pattern("fey", -1)
        self.assertListEqual(registry.url_patterns, ["baz", "fey", "bar", "foo"])

    def test_nested_patterns(self):
        registry1 = page.ViewRegistry()
        registry = page.ViewRegistry()
        registry.add_pattern("foo")
        registry.add_pattern(registry1)
        registry1.add_pattern("bar")
        registry1.add_pattern("baz")
        self.assertListEqual(registry.url_patterns, ["foo", "bar", "baz"])

    def test_add_url(self):
        with patch.object(page, "url", side_effect=_mocked_reverse):
            registry = page.ViewRegistry()
            registry.weighted_add_url(1, "bar")
            registry.add_url("foo")
            self.assertListEqual(registry.url_patterns, ["http://foo", "http://bar"])

    def test_register(self):
        with patch.object(page, "url", side_effect=lambda x, *a, **kw: str(x)):
            registry = page.ViewRegistry()
            registry.register("foo")(page.Page)
            self.assertListEqual(registry.url_patterns, ["foo"])
