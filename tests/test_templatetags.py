"""
\file

\author Mattia Basaglia

\copyright Copyright 2016 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

from mock import MagicMock, patch

from django.test import SimpleTestCase
from django.test.client import RequestFactory
from django.template import Template, Context, TemplateSyntaxError

from ..templatetags import simple_page as tt


class TestTemplateTags(SimpleTestCase):
    def test_make_attrs(self):
        self.assertEqual(
            tt.make_attrs({"a": 1, "b": 2, "c": 3}),
            "a='1' b='2' c='3'"
        )
        self.assertEqual(
            tt.make_attrs({"a": '"foo"'}),
            "a='&quot;foo&quot;'"
        )

    def test_section_header(self):
        self.assertEqual(
            tt.section_header("foo", "h1"),
            "<h1 id='foo' class='section'><a href='#foo'>foo</a></h1>"
        )
        self.assertEqual(
            tt.section_header("foo", "h1", **{"class": "suction"}),
            "<h1 id='foo' class='suction'><a href='#foo'>foo</a></h1>"
        )
        self.assertEqual(
            tt.section_header("foo", "h1", "bar"),
            "<h1 id='bar' class='section'><a href='#bar'>foo</a></h1>"
        )
        self.assertEqual(
            tt.section_header("foo", "h1", foo="bar"),
            "<h1 id='foo' class='section' foo='bar'><a href='#foo'>foo</a></h1>"
        )

    def test_section_plain(self):
        template = Template("{% load simple_page %}{% section h1 %}foo{% endsection %}")
        self.assertEqual(
            template.render(Context({})),
            "<h1 id='foo' class='section'><a href='#foo'>foo</a></h1>"
        )

    def test_section_as(self):
        template = Template(
            "{% load simple_page %}{% section h1 as foo %}foo{% endsection %}"
        )
        context = Context({})
        template.render(context)
        self.assertEqual(
            context["foo"],
            "<h1 id='foo' class='section'><a href='#foo'>foo</a></h1>"
        )

    def test_section_args(self):
        template = Template(
            "{% load simple_page %}{% section h1 'bar'%}foo{% endsection %}"
        )
        self.assertEqual(
            template.render(Context({})),
            "<h1 id='bar' class='section'><a href='#bar'>foo</a></h1>"
        )

    def test_section_kwargs(self):
        template = Template(
            "{% load simple_page %}{% section h1 id='bar'%}foo{% endsection %}"
        )
        self.assertEqual(
            template.render(Context({})),
            "<h1 id='bar' class='section'><a href='#bar'>foo</a></h1>"
        )

    def test_section_bad_args(self):
        self.assertRaises(TemplateSyntaxError, lambda: Template(
            "{% load simple_page %}{% section h1 foo++%}foo{% endsection %}"
        ))

    def test_link(self):
        factory = RequestFactory()
        ctx = {"request": factory.get("foo")}

        self.assertEqual(tt.link(ctx, "/bar"), "<a href='/bar'>/bar</a>")
        self.assertEqual(
            tt.link(ctx, "/foo"),
            "<span class='current_link'>/foo</span>"
        )
        self.assertEqual(tt.link(ctx, "bar", "baz"), "<a href='bar'>baz</a>")
        self.assertEqual(
            tt.link(ctx, "/foo", **{"class": "foo"}),
            "<span class='foo current_link'>/foo</span>"
        )
        self.assertEqual(
            tt.link(ctx, "/bar", foo="bar"),
            "<a foo='bar' href='/bar'>/bar</a>"
        )

    def test_link_link(self):
        factory = RequestFactory()
        ctx = {"request": factory.get("")}

        link = tt.Link("/foo", "bar")
        self.assertEqual(
            tt.link(ctx, link, foo="bar"),
            "<a foo='bar' href='/foo'>bar</a>"
        )

        link = tt.Link("/foo", "bar", lambda x: False)
        self.assertEqual(tt.link(ctx, link, foo="bar"), "")

    def test_flatten_list(self):
        self.assertEqual(tt.flatten_list(["foo", "bar"]), "foo bar")
        self.assertEqual(tt.flatten_list(["foo", "bar"], "_"), "foo_bar")
        self.assertEqual(tt.flatten_list(["foo", "bar"], "_", "x{}"), "xfoo_xbar")

    def test_invoke(self):
        self.assertEqual(tt.invoke("foo", "replace", "o", "u"), "fuu")

    def test_eval(self):
        ctx = {"foo": 3}
        self.assertEqual(tt.eval_python(ctx, "foo + 4"), 7)

    def test_if_crumb(self):
        ctx = {"page": MagicMock(breadcrumbs=["/foo"])}
        self.assertEqual(tt.if_crumb(ctx, "/foo", "foo"), "foo")
        self.assertEqual(tt.if_crumb(ctx, "/bar", "bar"), "")

    def test_api_url_link(self):
        with patch.object(tt, "reverse", side_effect=lambda x, **kw: "http://" + x):
            self.assertEqual(
                tt.api_url_link("foo"),
                "<p>Url: <a href='http://foo' class='api_url'>http://foo</a></p>"
            )
